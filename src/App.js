import React from 'react'
import Button from './Button'
import UserInput from './userInput'
import './App.css'
import Footer from './Footer'



class App extends React.Component{

    constructor(props){
        super(props)
        this.state = {randomNum: '', click: false }
        
        this.changeState = this.changeState.bind(this);
      }

    changeState(){
        const num = Math.floor(Math.random() * 1000000)

        this.setState({
            randomNum: num,
            click: true
            
        })

    }

    render(){ 
        setTimeout(()=>{
            this.myDiv.style.display='none'
        }, 5000)   
        

        return(
            <div className='center' >
                <div >
                <h1>Generate the Number</h1>
                <div ref={(ele) => this.myDiv = ele}>
                <p>{this.state.randomNum}</p>
                {(this.state.click === true) ? <p>Refreshing page in 5 seconds</p>: ''}
                </div><br/>



                <Button onClick={this.changeState}/><br/>

                <UserInput number={this.state.randomNum} click={this.state.click}/>

                

            </div>
                

                <Footer/>
               
                
            </div>
        );

    }

}

export default App