import React from 'react'
import './App'


class userInput extends React.Component{
    constructor(props){
        super(props)
        this.state = {userInput: 0, counter: 0, }

        this.guess = this.guess.bind(this)
        this.handleClick = this.handleClick.bind(this)
      }
      guess(){
        if(this.state.userInput === 0){
            alert('Enter a Number')
        }

        if(this.state.userInput === this.props.number){
            this.setState({counter: this.state.counter + 1 })
            
        }
        else{
            this.setState({counter: this.state.counter - 1})

        }
  
      }

     handleClick = e =>{
         const input = parseInt(e.target.value);
         this.setState({userInput: input})
     }

     refresh = () =>{
        window.location.reload();
     }

      

    render(){

        if(this.props.click === true){
            return(
                
            <div ref={(ele) => this.myDiv = ele}>
                <div className="ui large icon input">
                 <input type='text' onChange={this.handleClick} />
                 <i className="edit outline icon"></i>
                </div>
                

                <div className='btn'> 
                <button className='ui primary basic button' onClick={this.guess}>Guess</button>
                <button className='ui negative basic button' onClick={this.refresh}>Replay Game</button>
                </div>
                {this.state.counter >0 ? <h3>You have {this.state.counter} correct guess</h3> : ''}
            {this.state.counter <0 ? <h3>You are score is in Negative {this.state.counter}</h3> : ''}
    
            </div>
            );
        }
        else return null
    }
    
}

export default userInput