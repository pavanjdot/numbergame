import React from 'react'

class  Button extends React.Component {
    render(){
        return(
            <div >
                 <button className='ui primary basic button' onClick={this.props.onClick}>Generate</button> 
                 
            </div>
        );
    }
}
   


export default Button;